#!/bin/sh
set -e

echo "\n--- kernel ----------------------------------------"
uname -a

echo "\n--- environment variables -------------------------"
env

exec "$@"
