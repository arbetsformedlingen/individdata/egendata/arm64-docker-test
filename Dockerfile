FROM node:20

COPY entrypoint.sh /

ENTRYPOINT [ "/entrypoint.sh" ]